 /*project.importSVG('open.svg', function(item) {
            //alert(item);
        });*/ 
        
        
        var values = {
            paths: 20,
            minPoints: 5,
            maxPoints: 15,
            minRadius: 30,
            maxRadius: 90
        };

        var hitOptions = {
        	type : ('path'),
            segments: true,
            stroke: false,
            fill: true,
            tolerance: 5
        };
        // var rect = new Path.Rectangle({
        //     point: [0, 0],
        //     size: [view.size.width, view.size.height],
        //     strokeColor: 'white',
        //     selected: true
        // });
        // rect.sendToBack();
        // rect.fillColor = '#ffffff';
        
        if (document.documentElement.clientWidth < 1100){
            project.options.handleSize = 12;    
        }else{
            project.options.handleSize = 8;    
        }
		
		
        var halfWidth = document.documentElement.clientWidth / 2;
        var halfHeight = document.documentElement.clientHeight / 2;
     
        var pathData = 'M272.9,164.6v135.1l-88.3-14l-31,14V164.6h42.7l54.7,28.5L272.9,164.6z';
        var path1 = new Path(pathData);
        path1.fillColor = '#DDE05B';
        path1.position.x = halfWidth + 50;
        path1.position.y = halfHeight + 50;
		path1.attach('click', function() { seleccionar(this); });
		path1.attach('doubleclick', function() { agrandar(this); });        

        var pathData2 = 'M118.3,223.9h23.9v76H22.9V224c0-31.8,25.4-59.3,57.2-59.3v21.1h38.2V223.9z';
        var path2 = new Path(pathData2);
        path2.fillColor = '#FDEC72';
        path2.position.x = halfWidth - 90;
        path2.position.y = halfHeight + 50;
		path2.attach('click', function() { seleccionar(this); });
		path2.attach('doubleclick', function() { agrandar(this); });
        
        var pathData3 = 'M239.8,151.7h-86.7V16.6h58.2c32.8,0,58.8,26.4,58.8,58.8c0,22.1-12.4,41.2-30.3,50.9V151.7z';
        var path3 = new Path(pathData3);
        path3.fillColor = '#E88AB8';
        path3.position.x = halfWidth + 50;
        path3.position.y = halfHeight - 100;        
		path3.attach('click', function() { seleccionar(this); });
		path3.attach('doubleclick', function() { agrandar(this); });


        var pathData4 = 'M80.1,151.3c-37.2,0-67.6-30.3-67.6-67.6c0-37.2,30.3-67.6,67.6-67.6c37.2,0,67.6,30.3,67.6,67.6 C147.7,121,117.4,151.3,80.1,151.3z';
        var path4 = new Path(pathData4);
        path4.fillColor = '#CBE9F5';
        path4.position.x = halfWidth - 90;
        path4.position.y = halfHeight - 100;

		path4.attach('click', function() { seleccionar(this); });
		path4.attach('doubleclick', function() { agrandar(this); });

		//project.activeLayer.selected = true;

        var segment, path;

        function onMouseDown(event) {
            segment = path = null;
            var hitResult = project.hitTest(event.point, hitOptions);

            if (event.modifiers.shift) {
                if (hitResult.type == 'segment') {
                    hitResult.segment.remove();
                };
                return;
            }

            if (hitResult) {
                path = hitResult.item;
                if (hitResult.type == 'segment') {
                    segment = hitResult.segment;
                } else if (hitResult.type == 'stroke') {
                    var location = hitResult.location;
                    segment = path.insert(location.index + 1, event.point);
                    path.smooth();
                }
                hitResult.item.bringToFront();
            }else{

                project.activeLayer.selected = false;
            }
        }
		

        function onMouseMove(event) {
            var hitResult = project.hitTest(event.point, hitOptions);
            //project.activeLayer.selected = false;
            //if (hitResult && hitResult.item)
                //hitResult.item.selected = true;
        }

        function onMouseDrag(event) {
            if (segment) {
                segment.point += event.delta;
                segment.selected = true;
                //path.smooth();
            } else if (path) {
                path.position += event.delta;   
            } 
        }
       
        function seleccionar(elem){
        	project.activeLayer.selected = false;
        	elem.selected = true;
			console.log(elem);	

			if(elem.hasHandles()){
				
			}else{
				console.log("no");
			}
        	// for(i=0; i < elem.segments.length; i++){
        	// 	//console.log(elem);
        	// }             
        }

        function agrandar(elem){
        	elem.scale(1.5);
        }
       
        //var point = path1.segments[0]._point._x;
        function onFrame(event) {   
            if(!project.activeLayer.selected){
			  for (var i = 5; i <= 5; i++) {
    				var sinValue = Math.sin(event.time * 3 + i);
     
    				path1.segments[i].point.x = sinValue * 20 + halfWidth + 50;
    				path2.segments[i + 2].point.x = sinValue * 10 + halfWidth - 45;
    				path3.segments[i - 1].point.x = sinValue * 10 + halfWidth + 100;
    				path4.segments[1].point.x = sinValue * 10 + halfWidth - 150;

  				}
  				path1.segments[2].point.y = sinValue * 20 + halfHeight + 95;
  				path2.segments[3].point.y = sinValue * 15 + halfHeight + 100;
                //path3.segments[i - 1].point.y = sinValue * 15 + halfHeight;
  				//path4.segments[0].point.y = sinValue * 25 + halfHeight - 90;

            }
        }
        	
